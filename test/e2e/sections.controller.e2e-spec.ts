import expect from 'expect';
import { Model } from 'mongoose';
import { initTestEnvironment } from '../helpers/init-test-environment';
import { TestEnvironmentInterface } from '../helpers/test-environment.interface';
import { SECTION__TEST_IDS, SECTION_TEST_DATA } from '../data/sections';

const sectionData = {
    slug: 'test-section',
    name: 'Test section',
};

describe('Sections', () => {
    let env: TestEnvironmentInterface;

    beforeAll(async () => {
        env = await initTestEnvironment();
        await env.clearCollection('Section');
        await env.populateSection();
    });

    describe('GET /exercises-tree/sections/slug/:slug', () => {
        it('should return section by slug', async done => {
            const { body } = await env.get(
                '/exercises-tree/sections/slug/test-section-1',
            );

            expect(body.slug).toBe('test-section-1');
            expect(body.name).toBe('Test section 1');
            expect(body._id).toBe(SECTION__TEST_IDS[0].toString());

            done();
        });

        it('should return 404 when wrong slug', async done => {
            const { body } = await env.get(
                '/exercises-tree/sections/slug/xxx',
                404,
            );

            expect(body.statusCode).toBe(404);
            expect(body.message).toBeDefined();
            done();
        });
    });

    describe('GET /exercises-tree/sections/:id', () => {
        it('should return section by id', async done => {
            const { body } = await env.get(
                `/exercises-tree/sections/${SECTION__TEST_IDS[0].toString()}`,
            );

            expect(body.slug).toBe('test-section-1');
            expect(body.name).toBe('Test section 1');
            expect(body._id).toBe(SECTION__TEST_IDS[0].toString());

            done();
        });

        it('should return 404 when wrong id', async done => {
            const { body } = await env.get(
                '/exercises-tree/sections/111111111111111111111111',
                404,
            );

            expect(body.statusCode).toBe(404);
            expect(body.message).toBeDefined();
            done();
        });
    });

    describe('GET /exercises-tree/sections', () => {
        it('should return all sections', async done => {
            const { body } = await env.get(`/exercises-tree/sections`);

            expect(body.length).toBe(3);

            done();
        });
    });

    describe('POST /exercises-tree/sections', () => {
        it('should create a new section', async done => {
            const { body: section } = await env.post(
                '/exercises-tree/sections',
                sectionData,
            );

            expect(section.slug).toBe(sectionData.slug);
            expect(section.name).toBe(sectionData.name);
            expect(section._id).toBeDefined();

            done();
        });

        it('should not create a new section with duplicated slug', async done => {
            const { body: section } = await env.post(
                '/exercises-tree/sections',
                { slug: 'test-section-1', name: 'Test section' },
                400,
            );

            expect(section.errors[0].path).toBe('slug');

            done();
        });

        it('should not create a new section without slug', async done => {
            const { body: section } = await env.post(
                '/exercises-tree/sections',
                { name: 'Test section' },
                400,
            );

            expect(section.errors[0].path).toBe('slug');

            done();
        });

        it('should not create a new section without name', async done => {
            const { body: section } = await env.post(
                '/exercises-tree/sections',
                { slug: 'ewifnsdofgndsofgjdsogjdf' },
                400,
            );

            expect(section.errors[0].path).toBe('name');

            done();
        });

        it('should not create a new section with too short name', async done => {
            const { body: section } = await env.post(
                '/exercises-tree/sections',
                { slug: 'ewifnsdofgndsofgjdsogjdf', name: 'AA' },
                400,
            );

            expect(section.errors[0].path).toBe('name');

            done();
        });
    });

    describe('PATCH /exercises-tree/sections/:id', () => {
        it('should update name', async done => {
            const name = 'PATCH new name';
            const { body: section } = await env.patch(
                `/exercises-tree/sections/${SECTION__TEST_IDS[0].toString()}`,
                { name },
            );

            expect(section.name).toBe(name);

            done();
        });

        it('should not update slug', async done => {
            const slug = 'new-slug-name';
            const { body: section } = await env.patch(
                `/exercises-tree/sections/${SECTION__TEST_IDS[0].toString()}`,
                { slug },
            );

            expect(section.slug).toBe(SECTION_TEST_DATA[0].slug);

            done();
        });
    });

    describe('DELETE /exercises-tree/sections/:id', () => {
        it('should remove section', async done => {
            const { body: section } = await env.delete(
                `/exercises-tree/sections/${SECTION__TEST_IDS[0].toString()}`,
            );

            expect(section._id).toBe(SECTION__TEST_IDS[0].toString());

            done();
        });
    });

    afterAll(async () => {
        await env.clearCollection('Section');
        await env.close();
    });
});
