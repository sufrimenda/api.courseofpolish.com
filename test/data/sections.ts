import Schema from 'mongoose';

export const SECTION__TEST_IDS = [
    new Schema.Types.ObjectId(),
    new Schema.Types.ObjectId(),
    new Schema.Types.ObjectId(),
];

export const SECTION_TEST_DATA = [
    {
        _id: SECTION__TEST_IDS[0],
        slug: 'test-section-1',
        name: 'Test section 1',
    },
    {
        _id: SECTION__TEST_IDS[1],
        slug: 'test-section-2',
        name: 'Test section 2',
    },
    {
        _id: SECTION__TEST_IDS[2],
        slug: 'test-section-3',
        name: 'Test section 3',
    },
];
