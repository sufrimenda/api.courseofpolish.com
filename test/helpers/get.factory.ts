import request from 'supertest';

export const getFactory = app => async (endpoint, status = 200) => {
    const response = await request(app.getHttpServer())
        .get(endpoint)
        .expect(status);

    return {
        ...response,
        body: JSON.parse(response.text),
    };
};
