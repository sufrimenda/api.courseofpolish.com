import request from 'supertest';

export const patchFactory = app => async (endpoint, data, status = 200) => {
    return await request(app.getHttpServer())
        .patch(endpoint)
        .set('x-auth', 'xxx')
        .send(data)
        .expect(status);
    // return {
    //     ...response,
    //     body: JSON.parse(response.text),
    // };
};
