import { testModuleFactory } from './test-module.factory';
import { Model } from 'mongoose';
import { Section } from '../../src/modules/exercises-tree/sections/section.interface';
import { postFactory } from './post.factory';
import { TestEnvironmentInterface } from './test-environment.interface';
import { SECTION_TEST_DATA } from '../data/sections';
import { getFactory } from './get.factory';
import { patchFactory } from './patch.factory';
import { deleteFactory } from './delete.factory';

export const initTestEnvironment = async (): Promise<
    TestEnvironmentInterface
> => {
    const module = await testModuleFactory();
    const app = module.createNestApplication();
    await app.init();
    const sectionModel = module.get<Model<Section>>('SectionModel');
    const close = async () => await app.close();

    const post = postFactory(app);
    const patch = patchFactory(app);
    const get = getFactory(app);
    const remove = deleteFactory(app);

    const clearCollection = async name => {
        const model = module.get(`${name}Model`);
        return await (model as any).remove(_ => _);
    };

    const populateSection = async () => {
        return await sectionModel.create(SECTION_TEST_DATA);
    };

    return {
        module,
        app,
        close,

        sectionModel,
        clearCollection,
        populateSection,

        post,
        get,
        patch,
        delete: remove,
    };
};
