import request from 'supertest';

export const deleteFactory = app => async (endpoint, status = 200) => {
    const response = await request(app.getHttpServer())
        .get(endpoint)
        .set('x-auth', 'xxx')
        .expect(status);

    return {
        ...response,
        body: JSON.parse(response.text),
    };
};
