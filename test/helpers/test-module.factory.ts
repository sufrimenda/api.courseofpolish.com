import { Test } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';

export const testModuleFactory = () =>
    Test.createTestingModule({
        imports: [AppModule],
    }).compile();
