import { INestApplication } from '@nestjs/common';
import { TestingModule } from '@nestjs/testing';
import { Section } from '../../src/modules/exercises-tree/sections/section.interface';
import { Model } from 'mongoose';

export interface TestEnvironmentInterface {
    app: INestApplication;
    module: TestingModule;
    sectionModel: Model<Section>;
    post(endpoint: string, data: any, status?: number);
    patch(endpoint: string, data: any, status?: number);
    get(endpoint: string, status?: number);
    delete(endpoint: string, status?: number);
    clearCollection(name: string);
    close();
    populateSection();
}
