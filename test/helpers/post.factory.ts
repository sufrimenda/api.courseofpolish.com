import request from 'supertest';

export const postFactory = app => async (endpoint, data, status = 201) => {
    const response = await request(app.getHttpServer())
        .post(endpoint)
        .set('x-auth', 'xxx')
        .send(data)
        .expect(status);

    return {
        ...response,
        body: JSON.parse(response.text),
    };
};
