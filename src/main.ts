import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { formDataMiddleware } from './middleware/form-data.middleware';
import { CONFIG } from './config';
import * as fs from 'fs';

async function bootstrap() {
    // upload folders check
    if (!fs.existsSync('./.tmp')) {
        fs.mkdirSync('./.tmp');
    }

    const app = await NestFactory.create(AppModule);
    app.enableCors();
    app.use(formDataMiddleware);
    await app.listen(CONFIG.PORT);
}

bootstrap();
