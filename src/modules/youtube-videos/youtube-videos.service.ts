import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { YoutubeVideo } from './youtube-video.interface';

@Injectable()
export class YoutubeVideosService {
    constructor(
        @InjectModel('YoutubeVideo')
        private readonly youtubeVideoModel: Model<YoutubeVideo>,
    ) {}

    async getYoutubeVideoById(id): Promise<YoutubeVideo> {
        const youtubeVideo = this.youtubeVideoModel.findById(id);

        if (!youtubeVideo) {
            throw new HttpException(
                `Youtube video '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return youtubeVideo;
    }

    async getYoutubeVideos(): Promise<YoutubeVideo[]> {
        return await this.youtubeVideoModel.find().sort({ order: 1 });
    }

    async createYoutubeVideo(youtubeVideoDTO): Promise<YoutubeVideo> {
        const youtubeVideo = new this.youtubeVideoModel(youtubeVideoDTO);
        return await youtubeVideo.save();
    }

    async updateYoutubeVideo(id, youtubeVideoDTO): Promise<YoutubeVideo> {
        const youtubeVideo = await this.youtubeVideoModel.findByIdAndUpdate(
            id,
            youtubeVideoDTO,
            {
                new: true,
            },
        );

        if (!youtubeVideo) {
            throw new HttpException(
                `Youtube video '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return youtubeVideo;
    }

    async removeYoutubeVideo(id): Promise<YoutubeVideo> {
        return await this.youtubeVideoModel.findByIdAndRemove(id);
    }
}
