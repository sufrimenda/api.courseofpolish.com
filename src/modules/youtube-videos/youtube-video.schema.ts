import { Schema } from 'mongoose';

export const YoutubeVideoSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    youtube: {
        type: String,
        trim: true,
    },
    cover: {
        type: String,
        trim: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
});
