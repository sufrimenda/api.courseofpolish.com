import { Document } from 'mongoose';

export interface YoutubeVideo extends Document {
    name: string;
    description: string;
    youtube: string;
    cover: string;
    visible: boolean;
    order: number;
}
