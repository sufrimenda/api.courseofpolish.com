export const YoutubeVideoDTO = [
    'name',
    'description',
    'youtube',
    'cover',
    'visible',
    'order',
];
