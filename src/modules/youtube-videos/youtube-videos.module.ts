import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { YoutubeVideoSchema } from './youtube-video.schema';
import { YoutubeVideosService } from './youtube-videos.service';
import { YoutubeVideosController } from './youtube-videos.controller';
import { SharedModule } from '../shared/shared.module';

@Module({
    imports: [
        SharedModule,
        MongooseModule.forFeature([
            { name: 'YoutubeVideo', schema: YoutubeVideoSchema },
        ]),
    ],
    controllers: [YoutubeVideosController],
    providers: [YoutubeVideosService],
})
export class YoutubeVideosModule {}
