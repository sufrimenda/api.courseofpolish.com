import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { YoutubeVideosService } from './youtube-videos.service';
import { pick } from 'lodash';
import { YoutubeVideo } from './youtube-video.interface';
import { YoutubeVideoDTO } from './youtube-video.dto';
import { SecurityService } from '../../services/security.service';

@Controller('youtube-videos')
export class YoutubeVideosController {
    constructor(
        private youtubeVideosS: YoutubeVideosService,
        private securityS: SecurityService,
    ) {}

    @Get()
    getAll(@Req() req): Promise<YoutubeVideo[]> {
        return this.youtubeVideosS
            .getYoutubeVideos()
            .then(this.securityS.firewall(req));
    }

    @Get(':id')
    getById(@Req() req, @Param() { id }): Promise<YoutubeVideo> {
        return this.youtubeVideosS
            .getYoutubeVideoById(id)
            .then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<YoutubeVideo> {
        const youtubeVideoDTO = pick(data, YoutubeVideoDTO);
        return this.youtubeVideosS.createYoutubeVideo(youtubeVideoDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<YoutubeVideo> {
        const youtubeVideoDTO = pick(data, YoutubeVideoDTO);
        return this.youtubeVideosS.updateYoutubeVideo(id, youtubeVideoDTO);
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<YoutubeVideo> {
        return this.youtubeVideosS.removeYoutubeVideo(id);
    }
}
