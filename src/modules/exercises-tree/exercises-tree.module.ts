import { ExercisesService } from './exercises/exercises.service';
import { ExercisesController } from './exercises/exercises.controller';
import { Module } from '@nestjs/common';
import { SectionsController } from './sections/sections.controller';
import { SectionsService } from './sections/sections.service';
import { ChaptersController } from './chapters/chapters.controller';
import { ChaptersService } from './chapters/chapters.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ExerciseSchema } from './exercises/exercise.schema';
import { ExercisesTreeController } from './exercises-tree.controller';
import { SectionSchema } from './sections/section.schema';
import { ChapterSchema } from './chapters/chapter.schema';
import { SharedModule } from '../shared/shared.module';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Exercise', schema: ExerciseSchema },
            { name: 'Chapter', schema: ChapterSchema },
            { name: 'Section', schema: SectionSchema },
        ]),
        SharedModule,
    ],
    controllers: [
        SectionsController,
        ChaptersController,
        ExercisesController,
        ExercisesTreeController,
    ],
    providers: [SectionsService, ChaptersService, ExercisesService],
})
export class ExercisesTreeModule {}
