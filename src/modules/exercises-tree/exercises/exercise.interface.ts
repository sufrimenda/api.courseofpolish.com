import { Document } from 'mongoose';
import { Chapter } from '../chapters/chapter.interface';

export interface Exercise extends Document {
    slug: string;
    name: string;
    description: string;
    type: string;
    youtube: string;
    cover: string;
    examples: any[];
    data: any;
    visible: boolean;
    order: number;
    chapter: Chapter;
    chapterSlug: string;
}
