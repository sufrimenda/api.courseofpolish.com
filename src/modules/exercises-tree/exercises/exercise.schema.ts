import { Schema } from 'mongoose';

export const ExerciseSchema = new Schema({
    slug: {
        type: String,
        required: true,
        trim: true,
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    type: {
        type: String,
        required: true,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    youtube: {
        type: String,
        trim: true,
    },
    cover: {
        type: String,
        trim: true,
    },
    chapter: {
        type: Schema.Types.ObjectId,
        ref: 'Chapter',
        required: true,
    },
    chapterSlug: {
        type: String,
        required: true,
        trim: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
    examples: [Schema.Types.Mixed],
    data: Schema.Types.Mixed,
});
