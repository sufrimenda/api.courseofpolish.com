export const ExerciseDTO = [
    'name',
    'slug',
    'type',
    'description',
    'chapter',
    'youtube',
    'cover',
    'examples',
    'data',
    'visible',
    'order',
];
