import { Exercise } from './exercise.interface';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ChaptersService } from '../chapters/chapters.service';

@Injectable()
export class ExercisesService {
    constructor(
        @InjectModel('Exercise')
        private readonly exerciseModel: Model<Exercise>,
        private chaptersS: ChaptersService,
    ) {}

    async getExerciseById(id) {
        const exercise = await this.exerciseModel.findById(id);

        if (!exercise) {
            throw new HttpException(
                `Exercise '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return exercise;
    }

    async getExerciseByChapterAndSlug({
        exerciseSlug,
        chapterSlug,
    }): Promise<Exercise> {
        const exercise = await this.exerciseModel
            .findOne({ slug: exerciseSlug, chapterSlug })
            .exec();

        if (!exercise) {
            throw new HttpException(
                `Exercise '${chapterSlug}/${exerciseSlug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return exercise;
    }

    async createExercise(exerciseDTO): Promise<Exercise> {
        const examples =
            exerciseDTO.examples && typeof exerciseDTO.examples === 'string'
                ? JSON.parse(exerciseDTO.examples)
                : exerciseDTO.examples || [];

        const chapterSlug = await this.chaptersS
            .getChapterById(exerciseDTO.chapter)
            .then(chapter => chapter.slug);

        const createdExercise = new this.exerciseModel({
            ...exerciseDTO,
            examples,
            chapterSlug,
        });

        return await createdExercise.save();
    }

    async updateExercise(id, exerciseDTO): Promise<Exercise> {
        const examples =
            exerciseDTO.examples && typeof exerciseDTO.examples === 'string'
                ? JSON.parse(exerciseDTO.examples)
                : exerciseDTO.examples;

        if (examples) {
            exerciseDTO = {
                ...exerciseDTO,
                examples,
            };
        }

        const exercise = await this.exerciseModel.findByIdAndUpdate(
            id,
            exerciseDTO,
            {
                new: true,
            },
        );

        if (!exercise) {
            throw new HttpException(
                `Exercise '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return exercise;
    }

    async removeExercise(id): Promise<Exercise> {
        const exercise = await this.exerciseModel.findByIdAndRemove(id);

        if (!exercise) {
            throw new HttpException(
                `Exercise '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return exercise;
    }
}
