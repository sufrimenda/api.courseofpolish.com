import { ExercisesService } from './exercises.service';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { Exercise } from './exercise.interface';
import { pick } from 'lodash';
import { ExerciseDTO } from './exercise.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('exercises-tree/exercises')
export class ExercisesController {
    constructor(
        private exercisesS: ExercisesService,
        private securityS: SecurityService,
    ) {}

    @Get(':id')
    getExerciseById(@Req() req, @Param() { id }): Promise<Exercise> {
        return this.exercisesS
            .getExerciseById(id)
            .then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<Exercise> {
        const exerciseDTO = pick(data, ExerciseDTO);
        return this.exercisesS.createExercise(exerciseDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<Exercise> {
        const exerciseDTO = pick(data, ExerciseDTO);
        return this.exercisesS.updateExercise(id, exerciseDTO);
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<Exercise> {
        return this.exercisesS.removeExercise(id);
    }
}
