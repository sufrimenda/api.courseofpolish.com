import { Schema } from 'mongoose';

export const ChapterSchema = new Schema({
    slug: {
        type: String,
        required: true,
        unique: true,
        minlength: 3,
        trim: true,
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    youtube: {
        type: String,
        trim: true,
    },
    cover: {
        type: String,
        trim: true,
    },
    icon: {
        type: String,
        trim: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
    section: {
        type: Schema.Types.ObjectId,
        ref: 'Section',
        required: true,
    },
});
