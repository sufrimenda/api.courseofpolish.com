import { Exercise } from '../exercises/exercise.interface';
import { Document } from 'mongoose';

export interface Chapter extends Document {
    slug: string;
    name: string;
    description: string;
    youtube: string;
    cover: string;
    visible: boolean;
    order: number;
    exercises: Exercise[];
}
