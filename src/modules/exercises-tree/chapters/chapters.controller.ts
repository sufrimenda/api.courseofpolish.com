import { Chapter } from './chapter.interface';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { ChaptersService } from './chapters.service';
import { pick } from 'lodash';
import { ExercisesService } from '../exercises/exercises.service';
import { Exercise } from '../exercises/exercise.interface';
import { ChapterDTO } from './chapter.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('exercises-tree/chapters')
export class ChaptersController {
    constructor(
        private chaptersS: ChaptersService,
        private exercisesS: ExercisesService,
        private securityS: SecurityService,
    ) {}

    @Get('slug/:slug')
    getChapterBySlug(@Req() req, @Param() { slug }): Promise<Chapter> {
        return this.chaptersS
            .getChapterBySlug(slug)
            .then(this.securityS.firewall(req))
            .then(chapter => ({
                ...chapter,
                exercises: this.securityS.firewall(req)(chapter.exercises),
            }));
    }

    @Get(':id')
    getChapterById(@Req() req, @Param() { id }): Promise<Chapter> {
        return this.chaptersS
            .getChapterById(id)
            .then(this.securityS.firewall(req))
            .then(chapter => ({
                ...chapter,
                exercises: this.securityS.firewall(req)(chapter.exercises),
            }));
    }

    @Get('slug/:chapterSlug/exercises/:exerciseSlug')
    getExerciseByChapterAndSlug(
        @Req() req,
        @Param() { chapterSlug, exerciseSlug },
    ): Promise<Exercise> {
        return this.exercisesS
            .getExerciseByChapterAndSlug({
                chapterSlug,
                exerciseSlug,
            })
            .then(this.securityS.firewall(req));
    }

    @Get()
    findAll(@Req() req): Promise<Chapter[]> {
        return this.chaptersS.getChapters().then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<Chapter> {
        const chapterDTO = pick(data, ChapterDTO);
        return this.chaptersS.createChapter(chapterDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<Chapter> {
        const chapterDTO = pick(data, ChapterDTO);
        return this.chaptersS.updateChapter(id, chapterDTO);
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<Chapter> {
        return this.chaptersS.removeChapter(id);
    }
}
