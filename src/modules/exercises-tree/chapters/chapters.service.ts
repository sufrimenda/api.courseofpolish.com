import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Chapter } from './chapter.interface';
import { ensureObjectId } from '../../../helpers/ensureObjectId.helper';

@Injectable()
export class ChaptersService {
    constructor(
        @InjectModel('Chapter') private readonly chapterModel: Model<Chapter>,
        @InjectModel('Exercise') private readonly exerciseModel: Model<Chapter>,
    ) {}

    async getChapterById(id): Promise<Chapter> {
        const [chapter] = await this.chapterModel
            .aggregate([
                {
                    $match: {
                        _id: ensureObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'exercises',
                        localField: '_id',
                        foreignField: 'chapter',
                        as: 'exercises',
                    },
                },
            ])
            .exec();

        if (!chapter) {
            throw new HttpException(
                `Chapter '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...chapter,
            exercises: [...chapter.exercises].sort((a, b) => a.order - b.order),
        };
    }

    async getChapterBySlug(slug): Promise<Chapter> {
        const [chapter] = await this.chapterModel
            .aggregate([
                {
                    $match: {
                        slug,
                    },
                },
                {
                    $lookup: {
                        from: 'exercises',
                        localField: '_id',
                        foreignField: 'chapter',
                        as: 'exercises',
                    },
                },
            ])
            .exec();

        if (!chapter) {
            throw new HttpException(
                `Chapter '${slug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...chapter,
            exercises: [...chapter.exercises].sort((a, b) => a.order - b.order),
        };
    }

    async getChapters(
        { visible } = { visible: undefined },
    ): Promise<Chapter[]> {
        return await this.chapterModel.find({ visible }).exec();
    }

    async createChapter(chapterDTO): Promise<Chapter> {
        const createdChapter = new this.chapterModel(chapterDTO);
        return await createdChapter.save();
    }

    async updateChapter(id, chapterDTO): Promise<Chapter> {
        const chapter = await this.chapterModel.findByIdAndUpdate(
            id,
            chapterDTO,
            {
                new: true,
            },
        );

        if (!chapter) {
            throw new HttpException(
                `Chapter '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        await this.exerciseModel.updateMany(
            { chapter },
            { chapterSlug: chapter.slug },
        );

        return chapter;
    }

    async removeChapter(id): Promise<Chapter> {
        const chapter = await this.getChapterById(id);
        const preRemoveCheck = this.preRemoveChapterCheck(chapter);
        if (!preRemoveCheck.status) {
            throw new HttpException(
                `Chapter '${chapter.name}' cannot be removed. Reason: ${
                    preRemoveCheck.reason
                }`,
                HttpStatus.UNPROCESSABLE_ENTITY,
            );
        }
        return await this.chapterModel.findByIdAndRemove(id);
    }

    preRemoveChapterCheck(chapter): { status: boolean; reason?: string } {
        return chapter.exercises.length === 0
            ? { status: true }
            : { status: false, reason: 'contains exercises' };
    }
}
