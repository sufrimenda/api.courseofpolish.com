export const ChapterDTO = [
    'name',
    'slug',
    'description',
    'section',
    'youtube',
    'cover',
    'icon',
    'visible',
    'order',
];
