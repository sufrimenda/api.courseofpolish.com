import { Controller, Get, Req } from '@nestjs/common';
import { SectionsService } from './sections/sections.service';
import { SecurityService } from '../../services/security.service';

@Controller('exercises-tree')
export class ExercisesTreeController {
    constructor(
        private sectionsS: SectionsService,
        private securityS: SecurityService,
    ) {}

    @Get()
    getExerciseTree(@Req() req) {
        return this.sectionsS
            .getExerciseTree()
            .then(this.securityS.firewall(req))
            .then(sections =>
                sections.map(section => ({
                    ...section,
                    chapters: this.securityS.firewall(req)(section.chapters),
                })),
            );
    }

    @Get('counts')
    getExercisesTreeCounts() {
        return this.sectionsS.getExercisesTreeCounts();
    }
}
