import { Section } from './section.interface';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { SectionsService } from './sections.service';
import { pick } from 'lodash';
import { SectionDTO } from './section.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('exercises-tree/sections')
export class SectionsController {
    constructor(
        private sectionsS: SectionsService,
        private securityS: SecurityService,
    ) {}

    @Get('slug/:slug')
    getSectionBySlug(@Req() req, @Param() { slug }): Promise<Section> {
        return this.sectionsS
            .getSectionBySlug(slug)
            .then(this.securityS.firewall(req))
            .then(section => ({
                ...section,
                chapters: this.securityS.firewall(req)(section.chapters),
            }));
    }

    @Get(':id')
    getSectionById(@Req() req, @Param() { id }): Promise<Section> {
        return this.sectionsS
            .getSectionById(id)
            .then(this.securityS.firewall(req))
            .then(section => ({
                ...section,
                chapters: this.securityS.firewall(req)(section.chapters),
            }));
    }

    @Get()
    findAll(@Req() req): Promise<Section[]> {
        return this.sectionsS.getSections().then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<Section> {
        const sectionDTO = pick(data, SectionDTO);
        return this.sectionsS.createSection(sectionDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<Section> {
        const sectionDTO = pick(data, SectionDTO);
        return this.sectionsS.updateSection(id, sectionDTO);
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<Section> {
        return this.sectionsS.removeSection(id);
    }
}
