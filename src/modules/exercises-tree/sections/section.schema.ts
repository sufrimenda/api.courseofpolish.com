import { Schema } from 'mongoose';

export const SectionSchema = new Schema({
    slug: {
        type: String,
        required: true,
        unique: true,
        minlength: 3,
        trim: true,
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
});
