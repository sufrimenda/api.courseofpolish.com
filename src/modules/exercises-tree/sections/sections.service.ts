import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Section } from './section.interface';
import { Model } from 'mongoose';
import { ensureObjectId } from '../../../helpers/ensureObjectId.helper';

@Injectable()
export class SectionsService {
    constructor(
        @InjectModel('Section') private readonly sectionModel: Model<Section>,
    ) {}

    async getExerciseTree() {
        const sections = await this.sectionModel
            .aggregate([
                {
                    $sort: { order: 1 },
                },
                {
                    $lookup: {
                        from: 'chapters',
                        localField: '_id',
                        foreignField: 'section',
                        as: 'chapters',
                    },
                },
            ])
            .exec();

        return sections.map(section => ({
            ...section,
            chapters: [...section.chapters].sort((a, b) => a.order - b.order),
        }));
    }

    async getSectionById(id): Promise<Section> {
        const [section] = await this.sectionModel
            .aggregate([
                {
                    $match: {
                        _id: ensureObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'chapters',
                        localField: '_id',
                        foreignField: 'section',
                        as: 'chapters',
                    },
                },
                {
                    $unwind: {
                        path: '$chapters',
                        preserveNullAndEmptyArrays: true,
                    },
                },
                {
                    $sort: { order: 1, 'chapters.order': 1 },
                },
                {
                    $lookup: {
                        from: 'exercises',
                        localField: 'chapters._id',
                        foreignField: 'chapter',
                        as: 'chapters.exercises',
                    },
                },
                {
                    $group: {
                        _id: {
                            _id: '$_id',
                            name: '$name',
                            slug: '$slug',
                            visible: '$visible',
                            order: '$order',
                        },
                        chapters: {
                            $push: {
                                _id: '$chapters._id',
                                name: '$chapters.name',
                                slug: '$chapters.slug',
                                exercises: '$chapters.exercises',
                                description: '$chapters.description',
                                cover: '$chapters.cover',
                                icon: '$chapters.icon',
                                youtube: '$chapters.youtube',
                                visible: '$chapters.visible',
                                order: '$chapters.order',
                            },
                        },
                    },
                },
                {
                    $project: {
                        _id: '$_id._id',
                        name: '$_id.name',
                        slug: '$_id.slug',
                        visible: '$_id.visible',
                        order: '$_id.order',
                        chapters: {
                            $filter: {
                                input: '$chapters',
                                as: 'chapter',
                                cond: {
                                    $ne: [
                                        { $type: '$$chapter.slug' },
                                        'missing',
                                    ],
                                },
                            },
                        },
                    },
                },
            ])
            .exec();

        if (!section) {
            throw new HttpException(
                `Section '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...section,
            chapters: section.chapters.map(chapter => ({
                ...chapter,
                exercises: [...chapter.exercises].sort(
                    (a, b) => a.order - b.order,
                ),
            })),
        };
    }

    async getSectionBySlug(slug): Promise<Section> {
        const [section] = await this.sectionModel
            .aggregate([
                {
                    $match: {
                        slug,
                    },
                },
                {
                    $lookup: {
                        from: 'chapters',
                        localField: '_id',
                        foreignField: 'section',
                        as: 'chapters',
                    },
                },
            ])
            .exec();

        if (!section) {
            throw new HttpException(
                `Section '${slug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...section,
            chapters: [...section.chapters].sort((a, b) => a.order - b.order),
        };
    }

    async getSections(): Promise<Section[]> {
        return await this.sectionModel.find().sort({ order: 1 });
    }

    async createSection(sectionDTO): Promise<Section> {
        const section = new this.sectionModel(sectionDTO);
        return await section.save();
    }

    async updateSection(id, sectionDTO): Promise<Section> {
        const section = await this.sectionModel.findByIdAndUpdate(
            id,
            sectionDTO,
            {
                new: true,
            },
        );

        if (!section) {
            throw new HttpException(
                `Section '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return section;
    }

    async removeSection(id): Promise<Section> {
        const section = await this.getSectionById(id);
        const preRemoveCheck = this.preRemoveSectionCheck(section);
        if (!preRemoveCheck.status) {
            throw new HttpException(
                `Section '${section.name}' cannot be removed. Reason: ${
                    preRemoveCheck.reason
                }`,
                HttpStatus.UNPROCESSABLE_ENTITY,
            );
        }
        return await this.sectionModel.findByIdAndRemove(id);
    }

    preRemoveSectionCheck(section): { status: boolean; reason?: string } {
        return section.chapters.length === 0
            ? { status: true }
            : { status: false, reason: 'contains chapters' };
    }

    getExercisesTreeCounts() {
        return this.sectionModel.aggregate([
            {
                $match: {
                    visible: true,
                },
            },
            {
                $lookup: {
                    from: 'chapters',
                    localField: '_id',
                    foreignField: 'section',
                    as: 'chapters',
                },
            },
            {
                $unwind: '$chapters',
            },
            {
                $match: {
                    'chapters.visible': true,
                },
            },
            {
                $project: {
                    _id: 1,
                    chapter: '$chapters._id',
                },
            },
            {
                $lookup: {
                    from: 'exercises',
                    localField: 'chapter',
                    foreignField: 'chapter',
                    as: 'exercises',
                },
            },
            {
                $unwind: '$exercises',
            },
            {
                $match: {
                    'exercises.visible': true,
                },
            },
            {
                $project: {
                    _id: 1,
                    chapter: 1,
                    exercise: '$exercises._id',
                    examples: { $size: '$exercises.examples' },
                },
            },
            {
                $group: {
                    _id: {
                        _id: '$_id',
                        chapter: '$chapter',
                    },
                    exercises: {
                        $push: {
                            _id: '$exercise',
                            examples: '$examples',
                        },
                    },
                },
            },
            {
                $project: {
                    _id: '$_id._id',
                    chapter: '$_id.chapter',
                    exercises: 1,
                },
            },
            {
                $group: {
                    _id: {
                        _id: '$_id',
                    },
                    chapters: {
                        $push: {
                            _id: '$chapter',
                            exercises: '$exercises',
                        },
                    },
                },
            },
            {
                $project: {
                    _id: '$_id._id',
                    chapters: 1,
                },
            },
        ]);
    }
}
