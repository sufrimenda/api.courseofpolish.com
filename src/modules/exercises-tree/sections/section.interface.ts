import { Chapter } from '../chapters/chapter.interface';
import { Document } from 'mongoose';

export interface Section extends Document {
    slug: string;
    name: string;
    visible: boolean;
    order: number;
    chapters: Chapter[];
}
