import { Body, Controller, Post } from '@nestjs/common';
import { NewsletterService } from './newsletter.service';

@Controller('newsletter')
export class NewsletterController {
    constructor(private newsletterS: NewsletterService) {}

    @Post('subscribe')
    subscribe(@Body() { email }): Promise<string> {
        if (email) {
            this.newsletterS.sendBook(email).catch(() => {
                console.log('Cannot send email to: ', email);
            });
        }

        return Promise.resolve('ok');
    }
}
