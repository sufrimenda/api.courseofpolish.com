import { Injectable } from '@nestjs/common';
import { MailerService } from '../../services/mailer.service';

@Injectable()
export class NewsletterService {
    constructor(private mailerS: MailerService) {}

    sendBook(email) {
        const html = `
        <h1>Thank you for your subscription!</h1>
        The book has been attached to this email.<br /><br />
        To learn more Polish language visit my <a href="https://www.youtube.com/courseofpolish">Youtube channel</a><br /><br />
        Pozdrawiam serdecznie i życzę powodzenia w nauce polskiego!<br/>
        Jarek<br /><br />
        <sub>To unsubscribe from the newsletter just let me know by answering this email. It's my official email, not a bot ;)</sub>
    `;

        return this.mailerS.send(email, 'Course of Polish Book', html, [
            {
                filename: 'Polish vocabulary - 1000 most common words.pdf',
                path:
                    './src/modules/newsletter/Polish vocabulary - 1000 most common words.pdf',
            },
        ]);
    }
}
