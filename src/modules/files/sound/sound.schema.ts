import { Schema } from 'mongoose';

export const SoundSchema = new Schema({
    slug: {
        type: String,
        required: true,
        unique: true,
        minlength: 1,
        trim: true,
    },
});
