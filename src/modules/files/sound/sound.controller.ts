import {
    Body,
    Controller,
    Delete,
    Get,
    HttpException,
    HttpStatus,
    Param,
    Patch,
    Post,
    Req,
    Res,
} from '@nestjs/common';
import { pick } from 'lodash';
import { SoundService } from './sound.service';
import { Sound } from './sound.interface';
import { SoundDTO } from './sound.dto';

@Controller('files/sounds')
export class SoundController {
    constructor(private soundS: SoundService) {}

    @Get('slug/:slug/file')
    async getSoundFile(@Param() { slug }, @Res() res) {
        const soundPath = await this.soundS.getSoundFilePath(slug);
        res.sendFile(soundPath);
    }

    @Get()
    findAll(@Req() req): Promise<Sound[]> {
        return this.soundS.getSounds();
    }

    @Post()
    create(@Body() data, @Req() req): Promise<Sound> {
        const soundDTO = pick(data, SoundDTO);
        if (!req.files || !req.files.sound) {
            throw new HttpException(
                'You must provide a mp3 file',
                HttpStatus.BAD_REQUEST,
            );
        }

        return this.soundS.createSound(soundDTO, req.files.sound);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data, @Req() req): Promise<Sound> {
        const soundDTO = pick(data, SoundDTO);
        return this.soundS.updateSound(
            id,
            soundDTO,
            req.files && req.files.sound,
        );
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<Sound> {
        return this.soundS.removeSound(id);
    }
}
