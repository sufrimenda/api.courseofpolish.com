import { Document } from 'mongoose';

export interface Sound extends Document {
    slug: string;
}
