import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Sound } from './sound.interface';
import * as fs from 'fs';

@Injectable()
export class SoundService {
    constructor(
        @InjectModel('Sound') private readonly soundModel: Model<Sound>,
        @Inject('CONFIG') private config,
    ) {}

    async getSoundFilePath(slug) {
        const sound = await this.soundModel.findOne({ slug });

        if (!sound) {
            throw new HttpException(
                `Sound '${slug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return `${this.config.UPLOAD_DIR}/sounds/${sound._id}.mp3`;
    }

    async getSounds(): Promise<Sound[]> {
        return await this.soundModel.find();
    }

    async createSound(soundDTO, file): Promise<Sound> {
        const sound = new this.soundModel(soundDTO);
        const oldPath = file.path;
        const newPath = `${this.config.UPLOAD_DIR}/sounds/${sound._id}.mp3`;

        try {
            await sound.save();
        } catch (e) {
            fs.unlinkSync(oldPath);
            throw e;
        }

        await new Promise(resolve => {
            fs.readFile(oldPath, (err, data) => {
                fs.writeFile(newPath, data, () => {
                    fs.unlink(oldPath, () => {
                        resolve();
                    });
                });
            });
        });

        return sound;
    }

    async updateSound(id, soundDTO, file): Promise<Sound> {
        const sound = await this.soundModel.findByIdAndUpdate(id, soundDTO, {
            new: true,
        });

        if (!sound) {
            throw new HttpException(
                `Sound '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        if (file) {
            const oldPath = file.path;
            const newPath = `${this.config.UPLOAD_DIR}/sounds/${sound._id}.mp3`;

            await new Promise(resolve => {
                fs.readFile(oldPath, (err, data) => {
                    fs.writeFile(newPath, data, () => {
                        fs.unlink(oldPath, () => {
                            resolve();
                        });
                    });
                });
            });
        }

        return sound;
    }

    async removeSound(id): Promise<Sound> {
        const sound = await this.soundModel.findByIdAndRemove(id);

        const path = `${this.config.UPLOAD_DIR}/sounds/${sound._id}.mp3`;

        await new Promise(resolve => {
            fs.unlink(path, () => {
                resolve();
            });
        });

        return sound;
    }
}
