import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from '../shared/shared.module';
import { SoundSchema } from './sound/sound.schema';
import { SoundService } from './sound/sound.service';
import { SoundController } from './sound/sound.controller';

@Module({
    imports: [
        SharedModule,
        MongooseModule.forFeature([{ name: 'Sound', schema: SoundSchema }]),
    ],
    controllers: [SoundController],
    providers: [SoundService],
})
export class FilesModule {}
