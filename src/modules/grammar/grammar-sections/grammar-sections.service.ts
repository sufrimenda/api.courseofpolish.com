import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ensureObjectId } from '../../../helpers/ensureObjectId.helper';
import { GrammarSection } from './grammar-section.interface';

@Injectable()
export class GrammarSectionsService {
    constructor(
        @InjectModel('GrammarSection')
        private readonly grammarSectionModel: Model<GrammarSection>,
    ) {}

    async getGrammarSectionById(id): Promise<GrammarSection> {
        const [grammarSection] = await this.grammarSectionModel
            .aggregate([
                {
                    $match: {
                        _id: ensureObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'grammarchapters',
                        localField: '_id',
                        foreignField: 'grammarSection',
                        as: 'grammarChapters',
                    },
                },
            ])
            .exec();

        if (!grammarSection) {
            throw new HttpException(
                `GrammarSection '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...grammarSection,
            grammarChapters: [...grammarSection.grammarChapters].sort(
                (a, b) => a.order - b.order,
            ),
        };
    }

    async getGrammarSectionBySlug(slug): Promise<GrammarSection> {
        const [grammarSection] = await this.grammarSectionModel
            .aggregate([
                {
                    $match: {
                        slug,
                    },
                },
                {
                    $lookup: {
                        from: 'grammarchapters',
                        localField: '_id',
                        foreignField: 'grammarSection',
                        as: 'grammarChapters',
                    },
                },
            ])
            .exec();

        if (!grammarSection) {
            throw new HttpException(
                `GrammarSection '${slug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...grammarSection,
            grammarChapters: [...grammarSection.grammarChapters].sort(
                (a, b) => a.order - b.order,
            ),
        };
    }

    async getGrammarSections(): Promise<GrammarSection[]> {
        return await this.grammarSectionModel.find().sort({ order: 1 });
    }

    async createGrammarSection(grammarSectionDTO): Promise<GrammarSection> {
        const grammarSection = new this.grammarSectionModel(grammarSectionDTO);
        return await grammarSection.save();
    }

    async updateGrammarSection(id, grammarSectionDTO): Promise<GrammarSection> {
        const grammarSection = await this.grammarSectionModel.findByIdAndUpdate(
            id,
            grammarSectionDTO,
            {
                new: true,
            },
        );

        if (!grammarSection) {
            throw new HttpException(
                `GrammarSection '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarSection;
    }

    async removeGrammarSection(id): Promise<GrammarSection> {
        const grammarSection = await this.getGrammarSectionById(id);
        const preRemoveCheck = this.preRemoveGrammarSectionCheck(
            grammarSection,
        );
        if (!preRemoveCheck.status) {
            throw new HttpException(
                `GrammarSection '${
                    grammarSection.name
                }' cannot be removed. Reason: ${preRemoveCheck.reason}`,
                HttpStatus.UNPROCESSABLE_ENTITY,
            );
        }
        return await this.grammarSectionModel.findByIdAndRemove(id);
    }

    preRemoveGrammarSectionCheck(
        grammarSection,
    ): { status: boolean; reason?: string } {
        return grammarSection.grammarChapters.length === 0
            ? { status: true }
            : { status: false, reason: 'contains grammarChapters' };
    }
}
