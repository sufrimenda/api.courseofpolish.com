import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { pick } from 'lodash';
import { GrammarSectionsService } from './grammar-sections.service';
import { GrammarSection } from './grammar-section.interface';
import { GrammarSectionDTO } from './grammar-section.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('grammar/sections')
export class GrammarSectionsController {
    constructor(
        private grammarSectionsS: GrammarSectionsService,
        private securityS: SecurityService,
    ) {}

    @Get('slug/:slug')
    getGrammarSectionBySlug(
        @Req() req,
        @Param() { slug },
    ): Promise<GrammarSection> {
        return this.grammarSectionsS
            .getGrammarSectionBySlug(slug)
            .then(this.securityS.firewall(req))
            .then(grammarSection => ({
                ...grammarSection,
                grammarChapters: this.securityS.firewall(req)(
                    grammarSection.grammarChapters,
                ),
            }));
    }

    @Get(':id')
    getGrammarSectionById(
        @Req() req,
        @Param() { id },
    ): Promise<GrammarSection> {
        return this.grammarSectionsS
            .getGrammarSectionById(id)
            .then(this.securityS.firewall(req))
            .then(grammarSection => ({
                ...grammarSection,
                grammarChapters: this.securityS.firewall(req)(
                    grammarSection.grammarChapters,
                ),
            }));
    }

    @Get()
    findAll(@Req() req): Promise<GrammarSection[]> {
        return this.grammarSectionsS
            .getGrammarSections()
            .then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<GrammarSection> {
        const grammarSectionDTO = pick(data, GrammarSectionDTO);
        return this.grammarSectionsS.createGrammarSection(grammarSectionDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<GrammarSection> {
        const grammarSectionDTO = pick(data, GrammarSectionDTO);
        return this.grammarSectionsS.updateGrammarSection(
            id,
            grammarSectionDTO,
        );
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<GrammarSection> {
        return this.grammarSectionsS.removeGrammarSection(id);
    }
}
