import { Document } from 'mongoose';

export interface GrammarSection extends Document {
    slug: string;
    name: string;
    visible: boolean;
    order: number;
}
