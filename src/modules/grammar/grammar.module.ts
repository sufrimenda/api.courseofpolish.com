import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GrammarTopicsService } from './grammar-topics/grammar-topics.service';
import { GrammarSectionsService } from './grammar-sections/grammar-sections.service';
import { GrammarTopicsController } from './grammar-topics/grammar-topics.controller';
import { GrammarChaptersController } from './grammar-chapters/grammar-chapters.controller';
import { GrammarSectionsController } from './grammar-sections/grammar-sections.controller';
import { GrammarChaptersService } from './grammar-chapters/grammar-chapters.service';
import { GrammarSectionSchema } from './grammar-sections/grammar-section.schema';
import { GrammarChapterSchema } from './grammar-chapters/grammar-chapter.schema';
import { GrammarTopicSchema } from './grammar-topics/grammar-topic.schema';
import { SharedModule } from '../shared/shared.module';

@Module({
    imports: [
        SharedModule,
        MongooseModule.forFeature([
            { name: 'GrammarTopic', schema: GrammarTopicSchema },
            { name: 'GrammarChapter', schema: GrammarChapterSchema },
            { name: 'GrammarSection', schema: GrammarSectionSchema },
        ]),
    ],
    controllers: [
        GrammarSectionsController,
        GrammarChaptersController,
        GrammarTopicsController,
    ],
    providers: [
        GrammarSectionsService,
        GrammarChaptersService,
        GrammarTopicsService,
    ],
})
export class GrammarModule {}
