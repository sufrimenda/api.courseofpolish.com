export const GrammarChapterDTO = [
    'name',
    'slug',
    'grammarSection',
    'visible',
    'order',
];
