import { Document } from 'mongoose';

export interface GrammarChapter extends Document {
    slug: string;
    name: string;
    grammarSection: string;
    visible: boolean;
    order: number;
}
