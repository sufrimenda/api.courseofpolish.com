import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ensureObjectId } from '../../../helpers/ensureObjectId.helper';
import { GrammarChapter } from './grammar-chapter.interface';

@Injectable()
export class GrammarChaptersService {
    constructor(
        @InjectModel('GrammarChapter')
        private readonly grammarChapterModel: Model<GrammarChapter>,
    ) {}

    async getGrammarChapterById(id): Promise<GrammarChapter> {
        const [grammarChapter] = await this.grammarChapterModel
            .aggregate([
                {
                    $match: {
                        _id: ensureObjectId(id),
                    },
                },
                {
                    $lookup: {
                        from: 'grammartopics',
                        localField: '_id',
                        foreignField: 'grammarChapter',
                        as: 'grammarTopics',
                    },
                },
            ])
            .exec();

        if (!grammarChapter) {
            throw new HttpException(
                `GrammarChapter '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...grammarChapter,
            grammarTopics: [...grammarChapter.grammarTopics].sort(
                (a, b) => a.order - b.order,
            ),
        };
    }

    async getGrammarChapterBySlug(slug): Promise<GrammarChapter> {
        const [grammarChapter] = await this.grammarChapterModel
            .aggregate([
                {
                    $match: {
                        slug,
                    },
                },
                {
                    $lookup: {
                        from: 'grammartopics',
                        localField: '_id',
                        foreignField: 'grammarChapter',
                        as: 'grammarTopics',
                    },
                },
            ])
            .exec();

        if (!grammarChapter) {
            throw new HttpException(
                `GrammarChapter '${slug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return {
            ...grammarChapter,
            grammarTopics: [...grammarChapter.grammarTopics].sort(
                (a, b) => a.order - b.order,
            ),
        };
    }

    async getGrammarChapters(): Promise<GrammarChapter[]> {
        return await this.grammarChapterModel
            .find()
            .sort({ order: 1 })
            .exec();
    }

    async createGrammarChapter(grammarChapterDTO): Promise<GrammarChapter> {
        const grammarChapter = new this.grammarChapterModel(grammarChapterDTO);
        return await grammarChapter.save();
    }

    async updateGrammarChapter(id, grammarChapterDTO): Promise<GrammarChapter> {
        const grammarChapter = await this.grammarChapterModel.findByIdAndUpdate(
            id,
            grammarChapterDTO,
            {
                new: true,
            },
        );

        if (!grammarChapter) {
            throw new HttpException(
                `GrammarChapter '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarChapter;
    }

    async removeGrammarChapter(id): Promise<GrammarChapter> {
        const grammarChapter = await this.getGrammarChapterById(id);
        const preRemoveCheck = this.preRemoveGrammarChapterCheck(
            grammarChapter,
        );
        if (!preRemoveCheck.status) {
            throw new HttpException(
                `GrammarChapter '${
                    grammarChapter.name
                }' cannot be removed. Reason: ${preRemoveCheck.reason}`,
                HttpStatus.UNPROCESSABLE_ENTITY,
            );
        }
        return await this.grammarChapterModel.findByIdAndRemove(id);
    }

    preRemoveGrammarChapterCheck(
        grammarChapter,
    ): { status: boolean; reason?: string } {
        return grammarChapter.grammarTopics.length === 0
            ? { status: true }
            : { status: false, reason: 'contains grammar topics' };
    }
}
