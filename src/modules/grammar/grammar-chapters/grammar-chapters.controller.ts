import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { pick } from 'lodash';
import { GrammarChaptersService } from './grammar-chapters.service';
import { GrammarChapter } from './grammar-chapter.interface';
import { GrammarChapterDTO } from './grammar-chapter.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('grammar/chapters')
export class GrammarChaptersController {
    constructor(
        private grammarChaptersS: GrammarChaptersService,
        private securityS: SecurityService,
    ) {}

    @Get('slug/:slug')
    getGrammarChapterBySlug(
        @Req() req,
        @Param() { slug },
    ): Promise<GrammarChapter> {
        return this.grammarChaptersS
            .getGrammarChapterBySlug(slug)
            .then(this.securityS.firewall(req))
            .then(grammarChapter => ({
                ...grammarChapter,
                grammarTopics: this.securityS.firewall(req)(
                    grammarChapter.grammarTopics,
                ),
            }));
    }

    @Get(':id')
    getGrammarChapterById(
        @Req() req,
        @Param() { id },
    ): Promise<GrammarChapter> {
        return this.grammarChaptersS
            .getGrammarChapterById(id)
            .then(this.securityS.firewall(req))
            .then(grammarChapter => ({
                ...grammarChapter,
                grammarTopics: this.securityS.firewall(req)(
                    grammarChapter.grammarTopics,
                ),
            }));
    }

    @Get()
    findAll(@Req() req): Promise<GrammarChapter[]> {
        return this.grammarChaptersS
            .getGrammarChapters()
            .then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<GrammarChapter> {
        const grammarChapterDTO = pick(data, GrammarChapterDTO);
        return this.grammarChaptersS.createGrammarChapter(grammarChapterDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<GrammarChapter> {
        const grammarChapterDTO = pick(data, GrammarChapterDTO);
        return this.grammarChaptersS.updateGrammarChapter(
            id,
            grammarChapterDTO,
        );
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<GrammarChapter> {
        return this.grammarChaptersS.removeGrammarChapter(id);
    }
}
