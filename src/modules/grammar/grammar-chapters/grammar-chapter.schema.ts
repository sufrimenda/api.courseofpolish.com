import { Schema } from 'mongoose';

export const GrammarChapterSchema = new Schema({
    slug: {
        type: String,
        required: true,
        unique: true,
        minlength: 3,
        trim: true,
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    grammarSection: {
        type: Schema.Types.ObjectId,
        ref: 'GrammarSection',
        required: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
});
