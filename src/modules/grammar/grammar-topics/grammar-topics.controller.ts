import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Req,
} from '@nestjs/common';
import { pick } from 'lodash';
import { GrammarTopic } from './grammar-topic.interface';
import { GrammarTopicsService } from './grammar-topics.service';
import { GrammarTopicDTO } from './grammar-topic.dto';
import { SecurityService } from '../../../services/security.service';

@Controller('grammar/topics')
export class GrammarTopicsController {
    constructor(
        private grammarTopicsS: GrammarTopicsService,
        private securityS: SecurityService,
    ) {}

    @Get(':id')
    getGrammarTopicById(@Req() req, @Param() { id }): Promise<GrammarTopic> {
        return this.grammarTopicsS
            .getGrammarTopicById(id)
            .then(this.securityS.firewall(req));
    }

    @Get('slug/:slug')
    getGrammarTopicBySlug(
        @Req() req,
        @Param() { slug },
    ): Promise<GrammarTopic> {
        return this.grammarTopicsS
            .getGrammarTopicBySlug(slug)
            .then(this.securityS.firewall(req));
    }

    @Post()
    create(@Body() data): Promise<GrammarTopic> {
        const grammarTopicDTO = pick(data, GrammarTopicDTO);
        return this.grammarTopicsS.createGrammarTopic(grammarTopicDTO);
    }

    @Patch(':id')
    update(@Param() { id }, @Body() data): Promise<GrammarTopic> {
        const grammarTopicDTO = pick(data, GrammarTopicDTO);
        return this.grammarTopicsS.updateGrammarTopic(id, grammarTopicDTO);
    }

    @Delete(':id')
    remove(@Param() { id }): Promise<GrammarTopic> {
        return this.grammarTopicsS.removeGrammarTopic(id);
    }
}
