import { Document } from 'mongoose';
import { Chapter } from '../../exercises-tree/chapters/chapter.interface';

export interface GrammarTopic extends Document {
    slug: string;
    title: string;
    description: string;
    visible: boolean;
    order: number;
    grammarChapter: Chapter;
}
