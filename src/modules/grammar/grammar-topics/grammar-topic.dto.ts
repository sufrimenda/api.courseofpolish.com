export const GrammarTopicDTO = [
    'title',
    'slug',
    'description',
    'grammarChapter',
    'content',
    'visible',
    'order',
];
