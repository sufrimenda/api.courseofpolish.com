import { Schema } from 'mongoose';

export const GrammarTopicSchema = new Schema({
    slug: {
        type: String,
        required: true,
        trim: true,
        unique: true,
    },
    title: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    grammarChapter: {
        type: Schema.Types.ObjectId,
        ref: 'GrammarChapter',
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    visible: {
        type: Boolean,
    },
    order: {
        type: Number,
    },
});
