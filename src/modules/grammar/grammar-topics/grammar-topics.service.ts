import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GrammarTopic } from './grammar-topic.interface';

@Injectable()
export class GrammarTopicsService {
    constructor(
        @InjectModel('GrammarTopic')
        private readonly grammarTopicModel: Model<GrammarTopic>,
    ) {}

    async getGrammarTopicById(id) {
        const grammarTopic = await this.grammarTopicModel.findById(id);

        if (!grammarTopic) {
            throw new HttpException(
                `GrammarTopic '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarTopic;
    }

    async getGrammarTopicBySlug(grammarTopicSlug) {
        const grammarTopic = await this.grammarTopicModel.findOne({
            slug: grammarTopicSlug,
        });

        if (!grammarTopic) {
            throw new HttpException(
                `GrammarTopic '${grammarTopicSlug}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarTopic;
    }

    async createGrammarTopic(grammarTopicDTO): Promise<GrammarTopic> {
        const grammarTopic = new this.grammarTopicModel(grammarTopicDTO);

        return await grammarTopic.save();
    }

    async updateGrammarTopic(id, grammarTopicDTO): Promise<GrammarTopic> {
        const grammarTopic = await this.grammarTopicModel.findByIdAndUpdate(
            id,
            grammarTopicDTO,
            {
                new: true,
            },
        );

        if (!grammarTopic) {
            throw new HttpException(
                `GrammarTopic '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarTopic;
    }

    async removeGrammarTopic(id): Promise<GrammarTopic> {
        const grammarTopic = await this.grammarTopicModel.findByIdAndRemove(id);

        if (!grammarTopic) {
            throw new HttpException(
                `GrammarTopic '${id}' not found`,
                HttpStatus.NOT_FOUND,
            );
        }

        return grammarTopic;
    }
}
