import { Module } from '@nestjs/common';
import { MailerService } from '../../services/mailer.service';
import { CONFIG } from '../../config';
import { SecurityService } from '../../services/security.service';

@Module({
    imports: [],
    controllers: [],
    providers: [
        MailerService,
        SecurityService,
        {
            provide: 'CONFIG',
            useValue: CONFIG,
        },
    ],
    exports: [MailerService, SecurityService, 'CONFIG'],
})
export class SharedModule {}
