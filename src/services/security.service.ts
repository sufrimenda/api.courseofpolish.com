import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class SecurityService {
    firewall(req) {
        return entity => {
            if (req.authenticated) {
                return entity;
            }

            // TODO better filter this out when fetching from database
            if (Array.isArray(entity)) {
                return entity.filter(item => item.visible);
            }

            if (entity.visible) {
                return entity;
            }

            throw new HttpException(
                'Access denied, this entity has been hidden by admin',
                HttpStatus.FORBIDDEN,
            );
        };
    }
}
