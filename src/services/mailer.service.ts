import { Inject, Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';

@Injectable()
export class MailerService {
    transporter;

    constructor(@Inject('CONFIG') private config) {
        this.transporter = createTransport({
            service: 'gmail',
            auth: {
                user: config.EMAIL_USER,
                pass: config.EMAIL_PASSWORD,
            },
        });
    }

    send(to, subject, html, attachments) {
        return new Promise((resolve, reject) => {
            this.transporter.sendMail(
                {
                    to,
                    subject,
                    html,
                    attachments,
                },
                (err, info) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(info);
                    }
                },
            );
        });
    }
}
