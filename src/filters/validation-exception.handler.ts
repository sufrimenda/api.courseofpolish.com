import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';

@Catch()
export class ValidationExceptionHandler implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        if (!this.isValidationError(exception.name)) {
            throw exception;
        }
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const errors = this.parseException(exception);

        response.status(400).json({
            status: 'error',
            errors,
        });
    }

    isValidationError(error) {
        return error === 'ValidationError' || error === 'MongoError';
    }

    parseException(exception) {
        if (exception.name === 'ValidationError') {
            return this.parseValidationErrorException(exception);
        }

        if (exception.name === 'MongoError') {
            return this.parseMongoErrorException(exception);
        }
    }

    parseValidationErrorException(exception) {
        return Object.keys(exception.errors).map(field => ({
            path: field,
            message: exception.errors[field].message,
        }));
    }

    parseMongoErrorException(exception) {
        if (exception.message.indexOf('E11000') === -1) {
            throw exception;
        }

        const skippedBeginning = exception.message.split(' index: ')[1];
        const field = skippedBeginning.split('_')[0];

        return [
            {
                path: field,
                message: 'Probably duplicated key',
            },
        ];
    }
}
