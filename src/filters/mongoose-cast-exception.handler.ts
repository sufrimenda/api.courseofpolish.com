import {
    Catch,
    ExceptionFilter,
    ArgumentsHost,
    HttpStatus,
} from '@nestjs/common';
import { CastError } from 'mongoose';

@Catch(CastError)
export class MongooseCastExceptionHandler implements ExceptionFilter {
    catch(exception: CastError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = HttpStatus.BAD_REQUEST;

        response.status(status).json({
            message: exception.message,
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
        });
    }
}
