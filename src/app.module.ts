import { ValidationExceptionHandler } from './filters/validation-exception.handler';
import {
    MiddlewareConsumer,
    Module,
    NestModule,
    RequestMethod,
} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionHandler } from './filters/http-exception.handler';
import { MongooseCastExceptionHandler } from './filters/mongoose-cast-exception.handler';
import { CONFIG } from './config';
import { ExercisesTreeModule } from './modules/exercises-tree/exercises-tree.module';
import { NewsletterModule } from './modules/newsletter/newsletter.module';
import { NewsletterController } from './modules/newsletter/newsletter.controller';
import { SectionsController } from './modules/exercises-tree/sections/sections.controller';
import { ChaptersController } from './modules/exercises-tree/chapters/chapters.controller';
import { ExercisesController } from './modules/exercises-tree/exercises/exercises.controller';
import { YoutubeVideosModule } from './modules/youtube-videos/youtube-videos.module';
import { YoutubeVideosController } from './modules/youtube-videos/youtube-videos.controller';
import { GrammarModule } from './modules/grammar/grammar.module';
import { GrammarTopicsController } from './modules/grammar/grammar-topics/grammar-topics.controller';
import { GrammarSectionsController } from './modules/grammar/grammar-sections/grammar-sections.controller';
import { GrammarChaptersController } from './modules/grammar/grammar-chapters/grammar-chapters.controller';
import { AuthorizationMiddleware } from './middleware/authorization.middleware';
import { AuthenticationMiddleware } from './middleware/authentication.middleware';
import { FilesModule } from './modules/files/files.module';
import { SoundController } from './modules/files/sound/sound.controller';

if (process.env.NODE_ENV === 'test') {
    CONFIG.MONGO_URL = 'mongodb://localhost:27017/cop-test';
    CONFIG.AUTH_TOKEN = 'xxx';
}

@Module({
    imports: [
        ExercisesTreeModule,
        NewsletterModule,
        YoutubeVideosModule,
        GrammarModule,
        FilesModule,
        MongooseModule.forRoot(CONFIG.MONGO_URL),
    ],
    controllers: [],
    providers: [
        {
            provide: APP_FILTER,
            useClass: ValidationExceptionHandler,
        },
        {
            provide: APP_FILTER,
            useClass: HttpExceptionHandler,
        },
        {
            provide: APP_FILTER,
            useClass: MongooseCastExceptionHandler,
        },
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthenticationMiddleware)
            .with(CONFIG.AUTH_TOKEN)
            .forRoutes({ path: '*', method: RequestMethod.ALL });

        consumer
            .apply(AuthorizationMiddleware)
            .exclude(
                {
                    path: 'newsletter/subscribe',
                    method: RequestMethod.POST,
                },
                {
                    path: '*',
                    method: RequestMethod.GET,
                },
            )
            .forRoutes(NewsletterController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude({
                path: 'exercises-tree/sections/:id',
                method: RequestMethod.GET,
            })
            .forRoutes(SectionsController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude(
                {
                    path: 'exercises-tree/chapters',
                    method: RequestMethod.GET,
                },
                {
                    path: 'exercises-tree/chapters/slug/:slug',
                    method: RequestMethod.GET,
                },
                {
                    path: 'exercises-tree/chapters/:id',
                    method: RequestMethod.GET,
                },
                {
                    path:
                        'exercises-tree/chapters/slug/:chapterSlug/exercises/:exerciseSlug',
                    method: RequestMethod.GET,
                },
            )
            .forRoutes(ChaptersController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude({
                path: '*',
                method: RequestMethod.GET,
            })
            .forRoutes(ExercisesController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude({
                path: 'youtube-videos',
                method: RequestMethod.GET,
            })
            .forRoutes(YoutubeVideosController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude(
                {
                    path: 'grammar/sections',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/sections/:id',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/sections/slug/:slug',
                    method: RequestMethod.GET,
                },
            )
            .forRoutes(GrammarSectionsController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude(
                {
                    path: 'grammar/chapters',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/chapters/:id',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/chapters/slug/:slug',
                    method: RequestMethod.GET,
                },
            )
            .forRoutes(GrammarChaptersController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude(
                {
                    path: 'grammar/topics',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/topics/:id',
                    method: RequestMethod.GET,
                },
                {
                    path: 'grammar/topics/slug/:slug',
                    method: RequestMethod.GET,
                },
            )
            .forRoutes(GrammarTopicsController);

        consumer
            .apply(AuthorizationMiddleware)
            .exclude({
                path: 'files/sounds/slug/:slug/file',
                method: RequestMethod.GET,
            })
            .forRoutes(SoundController);
    }
}
