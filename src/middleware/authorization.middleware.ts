import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';

@Injectable()
export class AuthorizationMiddleware implements NestMiddleware {
    resolve(token): MiddlewareFunction {
        return (req, res, next) => {
            if (!req.authenticated) {
                return res.status(403).send({
                    status: 'error',
                    message: 'You are not authorized',
                });
            } else {
                next();
            }
        };
    }
}
