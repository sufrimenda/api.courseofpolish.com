import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
    resolve(token): MiddlewareFunction {
        return (req, res, next) => {
            const receivedToken = req.header('x-auth');

            if (receivedToken !== token) {
                req.authenticated = false;
            } else {
                req.authenticated = true;
            }
            next();
        };
    }
}
