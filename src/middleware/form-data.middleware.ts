import * as formidable from 'formidable';

export const formDataMiddleware = (req, res, next) => {
    const contentType = req.header('Content-Type');
    if (contentType && contentType.indexOf('multipart/form-data') !== -1) {
        const form = new formidable.IncomingForm({
            encoding: 'utf-8',
            uploadDir: './.tmp',
            multiples: true,
            keepExtensions: true,
        });
        form.once('error', console.log);
        form.parse(req, (err, body, files) => {
            Object.assign(req, { body, files });
            next();
        });
    } else {
        next();
    }
};
