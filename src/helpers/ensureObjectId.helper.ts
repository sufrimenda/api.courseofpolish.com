import * as mongoose from 'mongoose';

export const ensureObjectId = id =>
    typeof id === 'string' ? mongoose.Types.ObjectId(id) : id;
