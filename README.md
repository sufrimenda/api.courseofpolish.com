# cop-api

## Description

description

## Installation

- install mongodb
- `yarn`
- `cp src/config.ts.example src/config.ts`
- In config:
  - set the token
  - set email credentials
  - set database address
  - set port on which the API will listen
  - set upload directory
  
## Running the app

- start mongo
- run the app: `npm run start:dev`


## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

